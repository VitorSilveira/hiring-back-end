<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GitHubApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /*public function testListTest()
    {
        $this->get('api/users/teste')
        ->assertSuccessful()
        ->assertJsonStructure([
            "id",
            "login",
            "name",
            "avatar_url",
            "html_url"
         ]);             
        
    }*/

     /**
     * A basic test example.
     *
     * @return void
     */
    public function testReposTest()
    {
        $response = $this->get('api/users/teste/repos')
        ->assertSuccessful()
        ->assertJsonStructure([
           [ "id",
            "name",
            "description",
            "html_url"]
         ]);
        
    }
}
