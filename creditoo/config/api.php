<?php

/*
    Config of all API's
*/

return [
    'github' => [
        'url' => 'https://api.github.com/',
        'methods' => [
            'list' => [
              'url'     =>  'users/{user}',
              'verb'    =>  'GET'
            ],
            'repos' => [
                'url' => 'users/{user}/repos',
                'verb'  => 'GET',
            ]
        ]
    ]
];