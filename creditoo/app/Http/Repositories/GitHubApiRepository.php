<?php

namespace App\Http\Repositories;

use GuzzleHttp\Client;
use Illuminate\Http\Response;

class GitHubApiRepository {

    /** set instance of GuzzleHttp\Client **/
    private function set(){
        $url = config('api.github.url');
        $this->client = new Client(['base_uri' => $url, 'exceptions' => false]);  
    }

    /** Set Params of url Api 
     *  @param array $params
     *  @param string $url
     *  @return string
    **/
    private function setUrlParams($params, $url){

        foreach ($params as $param => $value) {
            $url = str_replace('{'. $param .'}', $value, $url);
        }
        
        return $url;
    }

    /** 
     * Call some method of API
     * @param array $method
     * @param array $params
     * @return array
    **/
    private function call($method, $params){

        $url = self::setUrlParams($params, $method['url']);
        
        self::set();
        $response = $this->client->request($method['verb'], $url); 
       
        $status = $response->getStatusCode();        
        $body = $response->getBody();
        $content = json_decode($body->getContents(), true);

        return ['status' => $status, 'content' => $content];
    }

    /** 
     * Call method list from GitHub Api users
     * @param string $user
     * @return Illuminate\Http\Response 
    **/
    public function list($user){
        
        $method = config('api.github.methods.list');
        $return =  self::call($method, ['user' => $user]);
       
        $response = [];
        if($return['status'] == '200'){ // if status == 200 OK
            
            $response = [
                'id'            => $return['content']['id'],
                'login'         => $return['content']['login'],
                'name'          => $return['content']['name'],
                'avatar_url'    => $return['content']['avatar_url'],
                'html_url'      => $return['content']['html_url'],
            ];
       }else{ // else return the code http and message of error from Api
            
            $response = [
                'status'    => $return['status'], 
                'message'   => $return['content']['message']
            ];

       }

        return response()->json($response, $return['status']);
    }

    /** 
     * Call method repos from GitHub Api users 
     * @param string $user
     * @return Illuminate\Http\Response 
    **/
    public function repos($user){

        $method = config('api.github.methods.repos');
        $return =  self::call($method, ['user' => $user]);

        $response = [];
        if($return['status'] == '200'){ // if status == 200 OK
            
            foreach ($return['content'] as $key => $repos) {
                $response[] = [
                    "id"            => $repos['id'],
                    "name"          => $repos['name'],
                    "description"   => $repos['description'],
                    "html_url"      => $repos['html_url'],
                ];        
            }
        }else{ // else return the code http and message of error from Api
            
            $response = [
                'status'    => $return['status'], 
                'message'   => $return['content']['message']
            ];

       }
        
        return response()->json($response);
    }

}

?>