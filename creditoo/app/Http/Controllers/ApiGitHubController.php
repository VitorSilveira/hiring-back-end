<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Repositories\GitHubApiRepository as Repository;

class ApiGitHubController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Repository $repository)
    {
       $this->repository = $repository;
    }
    
    /**
    *  List user from git hub
    */    
    public function list(Request $request, $user){

        return $this->repository->list($user);

    }

    /**
    *  Get repos from a user git hub
    */    
    public function repos(Request $request, $user){

        return $this->repository->repos($user);

    }
}
