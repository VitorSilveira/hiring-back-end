# Hiring Creditoo

## Requirements

- Laravel 5.5
- PHP 7.0

## Steps

- open the terminal and run the commands

	php artisan serve 
	php artisan config:cache
	
## Testing

- open the terminal and run the command

	phpunit tests/Feature/GitHubApiTest.php

